#include <iostream>

#include "Mono.hpp"

extern "C"
{
	void __declspec(dllexport) NativeHello()
	{
		std::cout << "Hello from native world!" << std::endl;
	}
}

int main()
{
//	mono::SetDirs("C:\\Program Files\\Mono\\bin", "C:\\Program Files\\Mono\\etc");
	mono::JitInit("monotest");
	mono::Assembly assm("managed_example.exe");
	auto image = assm.GetImage();
	auto mclass = image.ClassFromName("Test", "TestClass");
	auto hello = mclass.GetMethodFromName("ShowManagedHello", 0);
	auto printer = image.ClassFromName("Test", "Printer");
	auto basicPrint = image.ClassFromName("Test", "BasicPrint");
	auto advPrint = image.ClassFromName("Test", "AdvancedPrint");
	auto ymap = image.ClassFromName("Test", "YetMoreAdvancedPrint");
	auto ctor_bp = basicPrint.GetMethodFromName(".ctor", 0);
	auto ctor_adv = advPrint.GetMethodFromName(".ctor", 0);
	auto ctor_ymap = ymap.GetMethodFromName(".ctor", 0);
	auto bp_obj = basicPrint.ObjectNew();
	auto adv_obj = advPrint.ObjectNew();
	auto ymap_obj = ymap.ObjectNew();
	bp_obj.RuntimeInvoke(ctor_bp, NULL);
	adv_obj.RuntimeInvoke(ctor_adv, NULL);
	ymap_obj.RuntimeInvoke(ctor_ymap, NULL);
	auto virt = crap.GetMethodFromName("Print", 0);
//	auto virt = basicPrint.GetMethodFromName("Print", 0);
	bp_obj.RuntimeInvokeVirtual(virt, NULL);
	adv_obj.RuntimeInvokeVirtual(virt, NULL);
	ymap_obj.RuntimeInvokeVirtual(virt, NULL);
	mono::Object exception(nullptr);
	hello.RuntimeInvokeStatic(NULL, exception);
	mono::Object basic = basicPrint.ObjectNew();
	mono::JitCleanup();

	std::cin.get(); //prevents closing of the console
	return 0;
}