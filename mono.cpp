/*
MIT License

Copyright (c) 2018 Vlad "fakinator000"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <string>

#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>
#include <mono/metadata/mono-config.h>
#include "Mono.hpp"

MonoDomain * mono::Domain::domain;

void mono::SetDirs(const std::string & asmdir, const std::string & cfgdir)
{
	mono_set_dirs(asmdir.c_str(), cfgdir.c_str());
}

void mono::ConfigParse(const char * cfg)
{
	mono_config_parse(cfg);
}

void mono::JitInitVersion(const std::string & name, const std::string & version)
{
	mono::Domain::domain = mono_jit_init_version(name.c_str(), version.c_str());
}

void mono::JitInit(const std::string & name)
{
	mono::Domain::domain = mono_jit_init(name.c_str());
}

int mono::JitExec(const Assembly & assm, int argc, char * argv[])
{
	int retval = mono_jit_exec(mono::Domain::domain, assm.GetPtr(), argc, argv);
	return retval;
}

void mono::JitCleanup()
{
	mono_jit_cleanup(mono::Domain::domain);
	mono::Domain::domain = nullptr;
}

void mono::AddInternalCall(const std::string & name, const void * method)
{
	mono_add_internal_call(name.c_str(), method);
}

mono::Assembly mono::AssemblyOpen(const std::string & file)
{
	MonoAssembly * assm = mono_domain_assembly_open(mono::Domain::domain, file.c_str());
	return assm;
}

void mono::AssemblyClose(Assembly & assm)
{
	assm.Close();
}

void mono::Assembly::Open(const std::string & file)
{
	if(this->assembly != nullptr)
	{
		mono_assembly_close(this->assembly);
	}

	this->assembly = mono_domain_assembly_open(mono::Domain::domain, file.c_str());
	
}

void mono::Assembly::Close()
{
	if(this->assembly == nullptr)
	{
		return;
	}
	mono_assembly_close(this->assembly);
	this->assembly = nullptr;
}

mono::Image mono::Assembly::GetImage()
{
	auto *ptr = mono_assembly_get_image(this->assembly);
	return ptr;
}

mono::MClass mono::Image::ClassFromName(const std::string &mnamespace, const std::string & name)
{
	MonoClass *ptr = mono_class_from_name(this->image, mnamespace.c_str(), name.c_str());
	return ptr;
}

mono::Method mono::MClass::GetMethodFromName(const std::string & name, int param_count)
{
	mono::Method method = mono_class_get_method_from_name(this->mclass, name.c_str(), param_count);
	return method;
}

mono::ClassField mono::MClass::GetFieldFromName(const std::string & name)
{
	mono::ClassField field = mono_class_get_field_from_name(this->mclass, name.c_str());
	return field;
}

mono::Property mono::MClass::GetPropertyFromName(const std::string & name)
{
	mono::Property property = mono_class_get_property_from_name(this->mclass, name.c_str());
	return property;
}

mono::Object mono::MClass::ObjectNew()
{
	Object object = mono_object_new(mono::Domain::domain, this->mclass);
	return object;
}

void mono::Object::Free() 
{
	mono_free(object); 
}

//EXCEPTIONAL
mono::Object mono::Object::RuntimeInvoke(const mono::Method & method, void ** params, mono::Object & exception)
{
	mono::Object retobj = mono_runtime_invoke(method.GetPtr(), this->object, params, &(exception.object));
	return retobj;
}

mono::Object mono::Object::RuntimeInvokeStatic(const mono::Method & method, void ** params, mono::Object & exception)
{
	mono::Object retobj = mono_runtime_invoke(method.GetPtr(), NULL, params, &(exception.object));
	return retobj;
}

mono::Object mono::Object::RuntimeInvokeVirtual(const mono::Method & method, void ** params, mono::Object & exception)
{
	mono::Method mthd = mono_object_get_virtual_method(this->object, method.GetPtr());

	return this->RuntimeInvoke(mthd, params, exception);
}

mono::Object mono::Object::RuntimeInvoke(const mono::Method & method, void ** params)
{
	mono::Object retobj = mono_runtime_invoke(method.GetPtr(), this->object, params, NULL);
	return retobj;
}

mono::Object mono::Object::RuntimeInvokeStatic(const mono::Method & method, void ** params)
{
	mono::Object retobj = mono_runtime_invoke(method.GetPtr(), NULL, params, NULL);
	return retobj;
}

mono::Object mono::Object::RuntimeInvokeVirtual(const mono::Method & method, void ** params)
{
	mono::Method mthd = mono_object_get_virtual_method(this->object, method.GetPtr());

	return this->RuntimeInvoke(mthd, params);
}

void mono::Object::SetField(const ClassField & field, void * value)
{
	mono_field_set_value(this->object, field.GetPtr(), value);
}

void mono::Object::GetField(const ClassField & field, void * value)
{
	mono_field_get_value(this->object, field.GetPtr(), value);
}

void mono::Object::SetProperty(const Property & property, void ** params, mono::Object & exception)
{
	mono_property_set_value(property.GetPtr(), this->object, params, &(exception.object));
}

mono::Object mono::Object::GetProperty(const Property & property, void ** params, mono::Object & exception)
{
	mono::Object obj = mono_property_get_value(property.GetPtr(), this->object, params, &(exception.object));
	return obj;
}

mono::Object mono::Method::RuntimeInvokeStatic(void ** params, mono::Object & exception)
{
	return mono_runtime_invoke(this->method, NULL, params, exception.GetPtrPtr());
}