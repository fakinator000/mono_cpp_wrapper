using System;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
namespace Test
{
    interface Printer
    {
        void Print();
    }

    class TestClass
    {
        [DllImport("__Internal", EntryPoint = "NativeHello")]
        public static extern void ShowNativeHello();

        public static void ShowManagedHello()
        {
            Console.Beep();
            Console.WriteLine("Hello from the managed world");
            Console.Beep();
            Console.WriteLine("Native world says: \n\n");

            ShowNativeHello();
        }

        public static void Main(String[] args) => ShowManagedHello();
    };

    class BasicPrint : Printer
    {
        public BasicPrint() => Console.WriteLine("BasicPrint .ctor");
        public virtual void Print() => Console.WriteLine("BasicPrint");

    }

    class AdvancedPrint : BasicPrint
    {
        public AdvancedPrint() => Console.WriteLine("Advanced Print .ctor");
        public override void Print() => Console.WriteLine("AdvancedPrint");
    }

    class YetMoreAdvancedPrint : AdvancedPrint
    {
        public YetMoreAdvancedPrint() => Console.WriteLine("YetMoreAdvancedPrint .ctor");
        public override void Print() => Console.WriteLine("YetMoreAdvancedPrint");
    }
}