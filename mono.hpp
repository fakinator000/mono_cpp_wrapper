/*
MIT License

Copyright (c) 2018 Vlad "fakinator000"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <string>

#include <mono/jit/jit.h>

#include <mono/metadata/assembly.h>

#define MONO_EXTERN	extern "C"
#ifdef _WIN32
	#define MONO_EXPORT	__declspec(dllexport)
#else
	#define MONO_EXPORT
#endif


namespace mono
{
	class Domain;
	class Assembly;
	class Image;
	class MClass;
	class Method;
	class Object;
	class ClassField;
	class Property;
	
	using Field = ClassField;
	void SetDirs(const std::string &assemblydir, const std::string &configdir);
	void ConfigParse(const char * cfg);
	void JitInitVersion(const std::string & name, const std::string & version);
	void JitInit(const std::string &name);
	int JitExec(const Assembly &assm, int argc, char * argv[]);
	void JitCleanup();
	void AddInternalCall(const std::string &name, const void * method);
	Assembly AssemblyOpen(const std::string &file);
	void AssemblyClose(Assembly &assm);
}

class mono::Domain
{
public:
	static MonoDomain * domain;
};

class mono::Assembly
{
	MonoAssembly * assembly;
public:

	Assembly(const Assembly &) = delete;

	Assembly(MonoAssembly * assm) : assembly(assm) {};
	Assembly(const std::string &file) : assembly(nullptr)
	{
		Open(file);
	}

	Assembly() : assembly(nullptr) {};

	void Open(const std::string &file);
	
	void Close();

	inline MonoAssembly* GetPtr() const
	{
		return assembly;
	}

	mono::Image GetImage();
};

class mono::Image
{
	MonoImage * image;
public:

	Image(MonoImage * img) : image(img) {};

	inline MonoImage* GetPtr() const { return image; };

	mono::MClass ClassFromName(const std::string &mnamespace,const std::string &name);

};

class mono::MClass
{
	MonoClass * mclass;
public:

	MClass(MonoClass * mcls) : mclass(mcls) {};

	inline MonoClass* GetPtr() const { return mclass; };

	mono::Method GetMethodFromName(const std::string& name, int param_count);
	mono::ClassField GetFieldFromName(const std::string &name);
	mono::Property GetPropertyFromName(const std::string &name);
	Object ObjectNew();
};

class mono::Object
{
	MonoObject * object;
public:

	Object(MonoObject * obj) : object(obj)
	{

	}

	inline MonoObject* GetPtr() const { return object; };

	inline MonoObject** GetPtrPtr() { return &object; };

	void Free();

	mono::Object RuntimeInvoke(const mono::Method & method, void **params, mono::Object &exception);
	mono::Object RuntimeInvokeStatic(const mono::Method & method, void **params, mono::Object &exception);
	mono::Object RuntimeInvokeVirtual(const mono::Method & method, void **params, mono::Object &exception);

	mono::Object RuntimeInvoke(const mono::Method & method, void **params);
	mono::Object RuntimeInvokeStatic(const mono::Method & method, void **params);
	mono::Object RuntimeInvokeVirtual(const mono::Method & method, void **params);

	void SetField(const ClassField &field, void * value);
	void GetField(const ClassField &field, void * value);
	inline int GetField(const ClassField &field)
	{
		int value;
		this->GetField(field, &value);
		return value;
	}

	void SetProperty(const Property &property, void ** params, mono::Object &exception);
	mono::Object GetProperty(const Property &property, void **params, mono::Object &exception);


};

class mono::Method
{
	MonoMethod * method;
public:

	Method(MonoMethod * mthd) : method(mthd) {};

	inline MonoMethod* GetPtr() const { return method; };

	mono::Object RuntimeInvoke(mono::Object & object, void **params, mono::Object &exception);

	mono::Object RuntimeInvokeStatic(void **params, mono::Object &exception);

	mono::Object RuntimeInvokeVirtual(mono::Object &object, void **params, mono::Object &exception);

	inline mono::Object operator()(mono::Object &object, void **params, mono::Object &exception)
	{
		return RuntimeInvoke(object, params, exception);
	}
};

class mono::ClassField
{
	MonoClassField * field;
public:

	ClassField(MonoClassField * fld) : field(fld) {};

	inline MonoClassField* GetPtr() const { return field; };

};

class mono::Property
{
	MonoProperty * property;
public:

	Property(MonoProperty *prprt) : property(prprt) {};

	inline MonoProperty * GetPtr() const { return property; };

};

//TODO: Create a .ipp and move these functions

inline mono::Object mono::Method::RuntimeInvoke(mono::Object & object, void **params, mono::Object &exception)
{
	return object.RuntimeInvoke(*this, params, exception);
}

inline mono::Object mono::Method::RuntimeInvokeVirtual(mono::Object &object, void **params, mono::Object &exception)
{
	return object.RuntimeInvokeVirtual(*this, params, exception);
}
